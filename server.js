// Load HTTP module
const express = require('express')
const opn = require('opn')
const app = express()
const PORT = 3000

app.get('/', (req, res) => {
    res.sendFile(__dirname+'/src/countries.html')
})

app.get('/index', (req, res) => {
    res.sendFile(__dirname+'/src/index.html')
})

app.listen(3000, () => {
    console.log(`Server is running on http://localhost:${PORT}`)

    // Opens the URL in the default browser
    opn(`http://localhost:${PORT}`);
})

// app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static('public'))