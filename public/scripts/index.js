// When the user clicks on <div>, open the popup
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
};

var ncd = {
  "US": "18,16,14.5,14.3,14.6",
  "UK": "16.4,14,12,11.2,10.9",
  "Switzerland": "12.6,10.9,9.7,8.7,8",
  "Sweden": "13.2,11.8,10.4,9.3,9",
  "Singapore": "16.8,13.3,11.3,10,9",  
  "Norway": "14.2,12.2,11.3,9.3,9",
  "NewZealand": "15.9,13,11.8,10.4,10",
  "Netherlands": "16,13.9,12.5,11.3,11",
  "Luxembourg": "15,13.1,11.5,9.6,10",
  "Japan": "11.4,10.4,9.5,8.6,8",
  "Ireland": "17.9,13.6,11,10.5,10",
  "Iceland": "13.4,11.8,9.8,9.3,9",
  "Germany": "16.1,14.1,13,12.5,12",
  "Finland": "14.6,12.9,11.8,10,10",
  "Denmark": "18.3,15,13.4,11.7,11",
  "Canada": "14.4,12.6,11,10,9.8",
  "Belgium": "15,13.5,12.8,11.6,11",
  "Austria": "15.3,12.7,12,11.5,11",
  "Australia": "13.1,11.4,10,9.3,9.1",
  "Russia": "37,37.1,30.4,25.9,25",
  "India": "26.6,25.1,24,23.5,23",
  "China": "21.5,19,18.1,17.4,17"
};

var ncdrank = {
  "US": "5,4,4,4,4",
  "UK": "16.4,14,12,11.2,4",
  "Switzerland": "21,21,21,21,21",
  "Sweden": "19,18,18,18,18",
  "Singapore": "7,12,14,14,15",  
  "Norway": "17,17,14,17,17",
  "NewZealand": "11,11,12,13,13",
  "Netherlands": "9,8,8,9,9",
  "Luxembourg": "14,13,13,15,15",
  "Japan": "22,22,22,22,22",
  "Ireland": "6,9,16,9,10",
  "Iceland": "18,19,20,20,20",
  "Germany": "10,9,8,8,8",
  "Finland": "15,14,12,12,12",
  "Denmark": "4,5,5,6,8",
  "Canada": "16,16,17,14,15",
  "Belgium": "13,12,10,10,10",
  "Austria": "12,16,9,9,9",
  "Australia": "20,20,19,19,19",
  "Russia": "1,1,1,1,1",
  "India": "2,2,2,2,2",
  "China": "3,3,3,3,3"
};

var hdi = {
  "US": ".885,.898,.914,.92,.922",
  "UK": ".867,.891,.905,.918,.92",
  "Switzerland": ".889,.905,.932,.942,.943"  ,
  "Sweden": ".897,.899,.905,.929,.932",
  "Singapore": ".819,.868,.909,.929,.93",  
  "Norway": ".917,.932,.942,.948,.951",
  "NewZealand": ".869,.888,.899,.914,.915",
  "Netherlands": ".876,.891,.91,.926,.928",
  "Luxembourg": ".855,.878,.889,.899,.904",
  "Japan": ".855,.873,.885,.905,.907",
  "Ireland": ".857,.896,.909,.929,.934",
  "Iceland": ".86,.889,.891,.927,.933",
  "Germany": ".868,.903,.921,.933,.934",
  "Finland": ".858,.895,.903,.915,.918",
  "Denmark": ".863,.903,.91,.926,.928",
  "Canada": ".867,.892,.902,.92,.922",
  "Belgium": ".873,.889,.903,.913,.915",
  "Austria": ".838,.855,.895,.903,.906",
  "Australia": "13.1,11.4,10,9.3,9.1",
  "Russia": ".72,.752,.78,.813,.815",
  "India": ".493,.535,.581,.627,.636",
  "China": ".594,.647,.706,.743,.748"
};

var colour = {
  "Australia" : "rgb(191, 105, 105)",
  "Austria" : "rgb(191, 128, 105)",
  "Belgium" : "rgb(191, 152, 105)",
  "Canada" : "rgb(191, 175, 105)",
  "China" : "rgb(183, 191, 105)",
  "Denmark" : "rgb(160, 191, 105)",
  "Finland" : "rgb(136, 191, 105)",
  "Germany" : "rgb(113, 191, 105)",
  "Iceland" : "rgb(105, 191, 121)",
  "India" : "rgb(105, 191, 144)",
  "Ireland" : "rgb(105, 191, 167)",
  "Japan" : "rgb(105, 191, 191)",
  "Luxembourg" : "rgb(105, 167, 191)",
  "Netherlands" : "rgb(105, 144, 191)",
  "New Zealand" : "rgb(105, 121, 191)",
  "Norway" : "rgb(113, 105, 191)",
  "Russia" : "rgb(136, 105, 191)",
  "Singapore" : "rgb(160, 105, 191)",
  "Sweden" : "rgb(183, 105, 191)",
  "Switzerland" : "rgb(191, 105, 175)",
  "UK" : "rgb(191, 105, 152)",
  "USA" : "rgb(191, 105, 128)"
};

var final = {
  "2000" : "91051,430859,31989,289115",
  "2005" : "94576,399848,36980,293441",
  "2010" : "101126,376857,35949,304368",
  "2015" : "117633,416350,44597,323024",
  "2000" : "121715,432100,46499,331774"
};

function goBack() {
  window.history.back();
};

$('document').ready(function() {
  var countryList = sessionStorage.getItem('countryList');
  var countries = JSON.parse(countryList);
  $(".country").attr('opacity', 0.1);
  $(".country-label").attr('opacity', 0);

  for(var key in countries) {
    $("." + key).attr('opacity', 1);
    $(".label-" + key).attr('opacity', 1);
    
    $(".data").append("<div class='row'><button type='button' class='btn' id='btnn' value='" + key + "' style='background:" + colour[key] + ";margin-bottom:10px;' class='button-style btn open' data-toggle='modal' data-target='#myModal'>" + key + "</button></div>");
  }

  $("#btnn").click(function() {
    var country = $("#btnn").attr("value");
    var ncddata = ncd[country].split(',');
    var ncdrankdata = ncdrank[country].split(',');
    var hdidata = hdi[country].split(',');
    // $(".modal-body").html("<table class='dat' style='width:100%'><tr><th></th><th>2000</th><th>2005</th><th>2010</th><th>2015</th></tr><tr><th>NCD Cases (% of population)</th><td>" + ncddata[0] + "</td><td>" + ncddata[1] + "</td><td>" + ncddata[2] + "</td><td>" + ncddata[3] + "</td></tr><tr><th>NCD Rank</th><td>" + ncdrankdata[0] + "</td><td>" + ncdrankdata[1] + "</td><td>" + ncdrankdata[2] + "</td><td>" + ncdrankdata[3] + "</td></tr><tr><th>HDI</th><td>" + hdidata[0] + "</td><td>" + hdidata[1] + "</td><td>" + hdidata[2] + "</td><td>" + hdidata[3] + "</td></tr></table>");
  });

  // var datazero = final[0].split(',');
  // var dataone = final[1].split(',');
  // var datatwo = final[2].split(',');
  // var datathree = final[3].split(',');

  var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
      text: "Trend of 4 types of NCD Cases from 2000 - 2015",
      fontSize: 20,
      padding: 10,
    },
    axisX: {
      interval: 5,
      intervalType: "year"
    },
    axisY:{
      // valueFormatString:"$#0bn",
      gridColor: "#B6B1A8",
      tickColor: "#B6B1A8"
    },
    data: [{
        type: "stackedArea",
        showInLegend: true,
        color: "#696661",
        name: "Chronic Obstructive Pulmonary Disease (COPD)",
        dataPoints: [
          { y: 91051, x: 2000 },
          { y: 94576, x: 2005 },
          { y: 101126, x: 2010 },
          { y: 117633, x: 2015 }
        ]
      },
      {        
        type: "stackedArea",
        showInLegend: true,
        name: "Cardiovascular Diseases",
        color: "#EDCA93",
        dataPoints: [
          { y: 430859,  x: 2000 },
          { y: 399848,  x: 2005 },
          { y: 376857, x: 2010 },
          { y: 416350, x: 2015 }
        ]
      },
      {        
        type: "stackedArea",
        showInLegend: true,
        name: "Diabetes Mellitus",
        color: "#695A42",
        dataPoints: [
          { y: 31989, x: 2000 },
          { y: 36980, x: 2005 },
          { y: 35949,x: 2010 },
          { y: 44597, x: 2015 }
        ]
      },
      {        
        type: "stackedArea",
        showInLegend: true,
        name: "Malignant Neoplasms",
        color: "#B6B1A8",
        dataPoints: [
          { y: 289115,  x: 2000 },
          { y: 293441, x: 2005 },
          { y: 304368, x: 2010 },
          { y: 323024, x: 2015 }
        ]
    }]
  });
  chart.render();

});