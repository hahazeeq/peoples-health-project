$('document').ready(function() {
    var counter = 0;
    var text = "";
    var countries = {};

    // Add to readonly input on submit button click
    $("#submit").click(function() {
        var txt = $("#countryText").val();
        if (counter === 0) {
            text += txt;
            $("#displayText").val(text);
            counter++;
        } else {
            text += ", " + txt;
            $("#displayText").val(text);
        }
        countries[txt] = txt;
        $("#countryText").val("");
    });


    // Error checking for Countries input
    $(".alphaonly").bind("keyup blur", function() {
        var node = $(this);
        node.val(node.val().replace(/[^a-z A-z ,]/g, ""));
    });

    // Clear readonly input
    $("#clear").click(function() {
        $('#displayText').val('');
        // alert(countries);
        text = "";
        counter = 0;
        countries = {};
    });

    $('#plot').click(function () {
        sessionStorage.setItem('countryList', JSON.stringify(countries));
        window.location.href = "http://localhost:3000/index";
    });
    // $(function() {
    //     $('[data-toggle="tooltip"]').tooltip();
    // });

});