# People's Health Project

### Setup
Use ```npm install``` for the necessary dependancies.

### Start server
Use ```npm start``` to start server.
Server opens ```countries.html``` in ```src``` folder by default.

### Server URL
```http://localhost:3000```

### Source file directory
>```src``` HTML Files<br />
>```public``` Contains exposed files/folders<br />
>```assets``` Images etc<br />
>```css``` CSS Files<br />
>```data``` CSV, JSON files<br />
```fonts``` Third-party fonts<br />
>```scripts``` Javascript files<br />